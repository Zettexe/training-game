extends CharacterBody2D

var block_sprites = [
	preload("res://sprites/block1.png"),
	preload("res://sprites/block2.png"),
	preload("res://sprites/block3.png"),
	preload("res://sprites/block4.png"),
	preload("res://sprites/block5.png"),
	preload("res://sprites/block6.png")
]
var block_collision = [
	PackedVector2Array([-1, -1, 1, -1, 1, 1, -1, 1]),
	PackedVector2Array([-1, -2, 0, -2, 0, 2, -1, 2]),
	PackedVector2Array([-2, 0, -1, 0, -1, -1, 1, -1, 1, 0, 0, 0, 0, 1, -2, 1]),
	PackedVector2Array([0, -2, 0, -1, -1, -1, -1, 1, 0, 1, 0, 0, 1, 0, 1, -2]),
	PackedVector2Array([-1, -2, -1, -1, 0, -1, 0, 1, 1, 1, 1, -2]),
	PackedVector2Array([-1, -2, -1, -1, 0, -1, 0, 1, 1, 1, 1, -2])
]

func _ready():
	set_block(2)

func set_block(blocknumber: int):
	$Sprite2D.texture = block_sprites[blocknumber]
	var shape = ConvexPolygonShape2D.new()
	shape.set_point_cloud(block_collision[blocknumber])
	$CollisionShape2D.shape = shape

func _input(event):
	var dir = Input.get_axis("ui_left", "ui_right")
	velocity.x = sign(dir) * 16
	var collision = move_and_collide(velocity, true, 0, true)
	if not collision:
		move_and_collide(velocity)
	velocity.x = 0
