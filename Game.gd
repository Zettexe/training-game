extends Node2D

# Constants
const TICK_TIME = 0.2
const CENTER_COORDS = Vector2i(4, -2)
const RESET_INPUT_DELAY = 20
const RESET_ROTATION_DELAY = 20

# Block definitions
const BLOCK_DUP = [Vector2i(0, 0), Vector2i(1, 0), Vector2i(0, 1), Vector2i(1, 1)]
const BLOCK = [BLOCK_DUP, BLOCK_DUP, BLOCK_DUP, BLOCK_DUP]

const IBLOCK = [
	[Vector2i(0, -1), Vector2i(0, 0), Vector2i(0, 1), Vector2i(0, 2)],
	[Vector2i(-1, 0), Vector2i(0, 0), Vector2i(1, 0), Vector2i(2, 0)],
	[Vector2i(1, -1), Vector2i(1, 0), Vector2i(1, 1), Vector2i(1, 2)],
	[Vector2i(-1, 1), Vector2i(0, 1), Vector2i(1, 1), Vector2i(2, 1)],
]

const SBLOCK = [
	[Vector2i( 0, 0), Vector2i(-1,  0), Vector2i(0, -1), Vector2i(1, -1)],
	[Vector2i( 0, 0), Vector2i( 0, -1), Vector2i(1,  0), Vector2i(1,  1)],
	[Vector2i( 0, 1), Vector2i(-1,  1), Vector2i(0,  0), Vector2i(1,  0)],
	[Vector2i(-1, 0), Vector2i(-1, -1), Vector2i(0,  0), Vector2i(0,  1)],
]

const ZBLOCK = [
	[Vector2i( 0, 0), Vector2i( 1, 0), Vector2i(0, -1), Vector2i(-1, -1)],
	[Vector2i( 0, 0), Vector2i( 0, 1), Vector2i(1,  0), Vector2i( 1, -1)],
	[Vector2i( 0, 1), Vector2i( 1, 1), Vector2i(0,  0), Vector2i(-1,  0)],
	[Vector2i(-1, 0), Vector2i(-1, 1), Vector2i(0,  0), Vector2i( 0, -1)],
]

const LBLOCK = [
	[Vector2i(0, 0), Vector2i( 0, -1), Vector2i( 0,  1), Vector2i(-1,  1)],
	[Vector2i(0, 0), Vector2i( 1,  0), Vector2i(-1,  0), Vector2i(-1, -1)],
	[Vector2i(0, 0), Vector2i( 0,  1), Vector2i( 0, -1), Vector2i( 1, -1)],
	[Vector2i(0, 0), Vector2i(-1,  0), Vector2i( 1,  0), Vector2i( 1,  1)],
]

const TBLOCK = [
	[Vector2i(0, 0), Vector2i(-1, 0), Vector2i(0, -1), Vector2i(1, 0)],
	[Vector2i(0, 0), Vector2i( 0, 1), Vector2i(0, -1), Vector2i(1, 0)],
	[Vector2i(0, 0), Vector2i(-1, 0), Vector2i(0,  1), Vector2i(1, 0)],
	[Vector2i(0, 0), Vector2i(-1, 0), Vector2i(0, -1), Vector2i(0, 1)],
]

const BLOCKS = [BLOCK, IBLOCK, SBLOCK, ZBLOCK, LBLOCK, TBLOCK]

# Game state variables
@onready var tilemap = $TileMap
@onready var Music = $Music
@onready var FX = $FX
var tile_coords = CENTER_COORDS
var timer = 0
var input_delay = 0
var rotation_delay = 0
var block_rotation = 0
@onready var active_block = randi_range(0, 5)
var tick_time = TICK_TIME
var completed_rows = []
var game_over = false

func _ready():
	set_block()

# Input handling
func _input(event):
	handle_movement(event, "left", Vector2i.LEFT)
	handle_movement(event, "right", Vector2i.RIGHT)
	handle_rotation(event, "rotate_left", block_rotation - 1)
	handle_rotation(event, "rotate_right", block_rotation + 1)
	if event.is_action_pressed("slam"):
		while true:
			if not move_block(Vector2i.DOWN):
				break

func handle_movement(event: InputEvent, action, direction):
	if event.is_action_released(action): return
	if event.is_action_pressed(action):
		input_delay = RESET_INPUT_DELAY
		move_block(direction)

func handle_rotation(event, action, rot):
	if event.is_action_released(action): return
	if event.is_action_pressed(action):
		rotation_delay = RESET_ROTATION_DELAY
		rotate_block(rot)

func move_block(dir: Vector2i) -> bool:
	if not check_collision(tile_coords + dir, block_rotation):
		tile_coords += dir
		set_block()
		return true
	return false

func rotate_block(rot: int) -> bool:
	rot = wrap(rot, 0, 4)
	var kickback_positions = [Vector2i.ZERO, Vector2i.LEFT, Vector2i.RIGHT, Vector2i.UP]
	for offset in kickback_positions:
		if not check_collision(tile_coords + offset, rot):
			tile_coords += offset
			block_rotation = rot
			set_block()
			return true
	return false

func _process(delta):
	timer += delta
	process_movement()
	process_rotation()
	handle_tick_time()
	if timer < tick_time: return
	handle_completed_rows()
	timer -= tick_time
	handle_block_movement()
	
	if game_over:
		process_mode = Node.PROCESS_MODE_DISABLED
		FX.stream = preload("res://sounds/Gameover2.0.wav")
		FX.playing = true
		Music.playing = false
		await get_tree().create_timer(5).timeout
		get_tree().reload_current_scene()

func process_movement():
	var direction = sign(Input.get_axis("left", "right"))
	if direction != 0 and not Input.is_action_just_pressed("left") and not Input.is_action_just_pressed("right") and not Input.is_action_just_released("left") and not Input.is_action_just_released("right"):
		if input_delay > 0:
			input_delay -= 1
		else:
			input_delay = RESET_INPUT_DELAY
			move_block(Vector2i(direction, 0))

func process_rotation():
	var rot = sign(Input.get_axis("rotate_left", "rotate_right"))
	if rot != 0 and not Input.is_action_just_pressed("rotate_left") and not Input.is_action_just_pressed("rotate_right") and not Input.is_action_just_released("rotate_left") and not Input.is_action_just_released("rotate_right"):
		if rotation_delay > 0:
			rotation_delay -= 1
		else:
			rotation_delay = RESET_ROTATION_DELAY
			rotate_block(block_rotation + rot)

func handle_tick_time():
	if Input.is_action_pressed("down"):
		tick_time = TICK_TIME / 2
	else:
		tick_time = TICK_TIME

func handle_completed_rows():
	for row_index in completed_rows:
		for y in range(row_index, 0, -1):
			for x in range(0, 10): 
				tilemap.set_cell(1, Vector2i(x, y), 1, tilemap.get_cell_atlas_coords(1, Vector2i(x, y - 1)))
		for x in range(0, 10):
			tilemap.set_cell(1, Vector2i(x, 0), 1)
	completed_rows.clear()

func handle_block_movement():
	if move_block(Vector2i.DOWN): return
	for tile in BLOCKS[active_block][block_rotation]:
		tilemap.set_cell(1, tile_coords + tile, 1, tilemap.get_cell_atlas_coords(2, tile_coords + tile))
	tilemap.clear_layer(2)
	tile_coords = CENTER_COORDS
	active_block = randi_range(0, 5)
	for y in range(0, 20):
		var is_completed_row = true
		for x in range(0, 10):
			if not tilemap.get_cell_tile_data(1, Vector2i(x, y)):
				is_completed_row = false
				break
		if is_completed_row:
			completed_rows.append(y)
	match completed_rows.size():
		1:
			FX.stream = preload("res://sounds/TwoLineClear.wav")
		2:
			FX.stream = preload("res://sounds/ThreeLineClear.wav")
		3:
			FX.stream = preload("res://sounds/FourLineClear.wav")
		4:
			FX.stream = preload("res://sounds/FiveLineClear.wav")
		_:
			FX.stream = preload("res://sounds/Putdown2.wav")
	FX.playing = true
	for row_index in completed_rows:
		for x in range(0, 10):
			tilemap.set_cell(1, Vector2i(x, row_index - 1), 1, map_tile(tilemap.get_cell_atlas_coords(1, Vector2i(x, row_index - 1)), true))
			tilemap.set_cell(1, Vector2i(x, row_index), 1)
			tilemap.set_cell(1, Vector2i(x, row_index + 1), 1, map_tile(tilemap.get_cell_atlas_coords(1, Vector2i(x, row_index + 1)), false))

# Block setting and collision
func set_block():
	tilemap.clear_layer(2)
	tilemap.set_cells_terrain_connect(2, offset_tiles(BLOCKS[active_block][block_rotation]), 0, active_block)

func offset_tiles(input) -> Array:
	var offset_tiles = []
	for i in input:
		offset_tiles.append(i + tile_coords)
	return offset_tiles

func check_collision(at: Vector2i, rot: int) -> bool:
	for tile in BLOCKS[active_block][rot]:
		var coords = tile + at
		if is_colliding(coords) and coords.y < 0:
			game_over = true
			return false
		if is_colliding(coords) or coords.y >= 20 or coords.x < 0 or coords.x >= 10:
			return true
	return false

func is_colliding(coords: Vector2i) -> bool:
	return tilemap.get_cell_tile_data(1, coords) != null

func map_tile(tile: Vector2i, top: bool = false) -> Vector2i:
	if (top and tile == Vector2i(1, 0)) or (not top and tile == Vector2i(3, 0)):
		return Vector2i(2, 1)
	if (top and tile == Vector2i(2, 0)) or (not top and tile == Vector2i(4, 0)):
		return Vector2i(3, 1)
	if tile == Vector2i(5, 0):
		return Vector2i(7, 6) if top else Vector2i(5, 6)
	if tile == Vector2i(6, 0):
		return Vector2i(6, 6) if top else Vector2i(8, 6)
	if (top and tile == Vector2i(8, 0)) or (not top and tile == Vector2i(7, 0)):
		return Vector2i(9, 6)
	
	for i in range(1, 7):
		if (top and tile == Vector2i(1, i)) or (not top and tile == Vector2i(4, i)):
			return Vector2i(0, i)
		if (top and tile == Vector2i(5, i)) or (not top and tile == Vector2i(7, i)):
			return Vector2i(2, i)
		if (top and tile == Vector2i(8, i)) or (not top and tile == Vector2i(6, i)):
			return Vector2i(3, i)
		if tile == Vector2i(10, i):
			return Vector2i(4, i) if top else Vector2i(1, i)
	
	return tile
